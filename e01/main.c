#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stack_pub.h"

int search(Item pTop, char* word);
void write(Item pTop, FILE* output);

int main(int argc, char *argv[])
{
    Item pTop = initStack();
    Item tmp;
    if (argc != 3){
        printf("Usage : %s input output.\n", argv[0]);
        exit(-1);
    }
    int i;
    char word[100];
    FILE * input = fopen(argv[1], "r");
    FILE * output = fopen(argv[2], "w");
    if(input == NULL || output == NULL)
    {
        printf("Failed opening files");
        exit(-1);
    }
    int posIn;



    while(fscanf(input, "%s", word) != EOF)
    {
        tmp = pTop;
        posIn = search(pTop, word);
        if (posIn >= 0){
            for(i=0; i<posIn; i++){
                tmp = tmp->next;
            }
            tmp->num++;
        }
        else
            pTop = push(pTop, word);
    }

    write(pTop, output);

    destroyStack(pTop);
    return 0;
}

int search(Item pTop, char* word)
{
    int pos = 0;
    while (pTop != NULL && pTop->next != NULL)
    {
        if(strcmp(pTop->val, word) == 0)
            return pos;

        pTop = pTop->next;
        pos++;
    }
    return -1;
}

void write(Item pTop, FILE* output)
{
    while(pTop != NULL)
    {
        printf("%s %d\n", pTop->val, pTop->num);
        pTop = pTop->next;
    }
    return;

}
