#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stack.h"


Item newE(char * word)
{
    Item pTop = (struct item *)malloc(sizeof(*pTop));
    if (pTop == NULL){
        printf("Fail creating element");
        exit(-1);
    }
    pTop->val = malloc((strlen(word)+1)*sizeof(char));
    strcpy(pTop->val, word);
    pTop->next = NULL;
    return pTop;
}

Item initStack()
{
    Item pTop = NULL;
    return pTop;
}

void destroyStack(Item pTop)
{
    if (pTop == NULL)
    {
        return;
    }
    if (pTop->next == NULL){
        freeItem(pTop);
        return;
    }
    destroyStack(pTop->next);
    freeItem(pTop);
    return;
}

void printStack(Item pTop)
{
    if (pTop == NULL){
        printf("\nStack is empty\n\n");
        return;
    }

    while (pTop->next != NULL)
    {
        printItem(pTop);
        printf(" -> ");
        pTop = pTop->next;
    }
    printf("Stack :\t");
    printItem(pTop);
    printf("\n\n");
    fflush(stdout);
    return;
}

Item push(Item pOld, char *word)
{
    Item pTop = newE(word);
    pTop->next = pOld;
    pTop->num = 1;
    return pTop;
}


Item pop(Item pTop)
{
    if (pTop == NULL){
        printf("Stack is empty\n");
        return NULL;
    }
    Item pOld;
    pOld = pTop;
    printf("\nOn pop ");
    printItem(pTop); printf("\n");   
    pTop = pTop->next;
    freeItem(pOld);
    return pTop;
}
