#ifndef STACK_H
#define STACK_H
#include "stack_string.h"

typedef struct item * Item;

Item newE();
Item initStack();
void destroyStack(Item pTop);
Item push(Item pOld, char *word);
void printStack(Item pTop);
Item pop(Item pTop);

/*to be define in a proper C file */
void freeItem(Item pTop);
void printItem(Item pTop);
void setItem();

#endif
