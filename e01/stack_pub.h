#ifndef STACK_PUB_H
#define STACK_PUB_H

#include <stdlib.h>
#include <stdio.h>
#include "stack.h"

typedef struct item * Item;

Item initStack();
void destroyStack(Item pTop);
void printStack(Item pTop);
Item push(Item pOld, char *word);
Item pop(Item pTop);

#endif
