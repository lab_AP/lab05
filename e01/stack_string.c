#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stack.h"

void freeItem(Item pTop)
{
    free(pTop->val);
    free(pTop);
    return;
}

void printItem(Item pTop)
{
    printf("%s", pTop->val);
    return;
}

void setItem(Item pTop)
{
    char value[100];
    printf("String a ajouter\n");
    scanf("%s", value);
    int k = strlen(value);
    pTop->val = (char *)malloc(k * sizeof(char));
    if (pTop->val == NULL){
        printf("Alloc de pTop->val failed\n");
        exit(-1);
    }
    strcpy(pTop->val, value);
    return;
}

